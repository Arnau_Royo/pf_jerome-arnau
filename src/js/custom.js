

let apiUrl = 'http://labs.iam.cat/api/api.php/records/equips';

let load_state, totalAules;

let today = new Date();
let newerDate = formatDate(today);
//let olderDate = formatDate(today.setMonth(today.getMonth()-1));
let olderDate = formatDate(today.setDate(today.getDate()-7));

Vue.component('aula-item', {
	props: ['aula'],
	template: `
	<div class="col-xs-6 col-md-3" data-toggle="modal" data-target="#">
			<div class="panel panel-default">
				<div class="panel-body">
					<div style="padding-bottom: 1em;"><em class="fa fa-xl fa-desktop color-blue" style="margin-right: 5%;"></em><span class="large">{{ aula.title }}</span></div>
					<h4>Ordinadors: {{ aula.ordinadors }}</h4>
					<h4>Registres: {{ aula.registres }}</h4>
				</div>
			</div>
		</div>
	</div>	`
})
Vue.component('aula-modal', {
	props: ['aula'],
	template:`<div class="modal fade bd-example-modal-lg" id="" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>	
		<h2 class="modal-title" id="exampleModalLongTitle">Aula {{aula.title}}</h2>
			<p>Ordinadors: {{ aula.ordinadors }}</p>
			<p>Registres: {{ aula.registres }}</p>
			
		</div>
		<div class="modal-body">
			<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">MAC</th>
      <th scope="col">Ram</th>
      <th scope="col">Data de Registre</th>
    </tr>
  </thead>
  <tbody>
    <tr v-for="taula in aula.dades">
      <th scope="row">{{taula.nom}}</th>
      <td>{{taula.mac}}</td>
      <td>{{taula.ram}}</td>
      <td>{{taula.data}}</td>
    </tr>
  </tbody>
</table>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Tancar</button>
		</div>
		</div>
	</div>
	</div>`,
	created: function() {
		load_state++;
		
		console.log(load_state + " items");
		$('.state').html("Aules generades: " + load_state + " de " + totalAules);

		if (load_state == totalAules-1) { //When the load_state is equal to the totalAules the preloader fades out.
			console.log('%c' + load_state+1 + " items, ESCONDO PRELOADER", 'background: darkgreen; color: white');
			$('.preloader').fadeOut('slow');
			
		}
	}
})

let aules = new Vue({
	el: '#aules',
	data: {
		aulesList: []
	}
})

new Vue({
	el: '#dateRange',
	methods: {
		today: function() {
			today = new Date();
			olderDate = formatDate(today.setHours(0,0,0,0));
			renderAules();
		},
		week: function() {
			today = new Date();
			olderDate = formatDate(today.setDate(today.getDate()-7));
			renderAules();
		},
		month: function() {
			today = new Date();
			olderDate = formatDate(today.setMonth(today.getMonth()-1));
			renderAules();
		},
		year: function() {
			today = new Date();
			olderDate = formatDate(today.setFullYear(today.getFullYear()-1));
			renderAules();
		}
	}
})

// Initial load
renderAules();

function renderAules() {
	aules.aulesList.length = 0;
	$('.preloader').fadeIn('slow');

	getAules().then(function(response) {
		totalAules = response.length;
		load_state = 0;
		console.log('%c' + totalAules + ' Aules a generar', 'background: grey; color: gold');

		response.forEach(async function(value) {
			let dades = await getDadesAula(value).then(function(response) {
				return response.records;
			});
			let ordinadors = dades.map(function(d) {
				return d.mac;
			});
			ordinadors = unique(ordinadors);
			let aula = { title: value, ordinadors: ordinadors.length, registres: dades.length, dades: dades };
			aules.aulesList.push(aula);
			aules.aulesList.sort(function (a, b) {
				if (a.title > b.title) {
					return 1;
				}
				if (a.title < b.title) {
					return -1;
				}
				// a must be equal to b
				return 0;
				});
		})
	});
}

// Get existing 'aules' within a month
async function getAules() {
	let dades = await getDadesDate(olderDate, newerDate);
	let aules = dades.records.map(function (d) { 
			return d.aula;
	});
	aules = unique(aules.sort());
	return aules;
}

// Get data between two dates
function getDadesDate(olderDate, newerDate) {
	return fetch(`${apiUrl}?filter=data,bt,${olderDate},${newerDate}`)
	.then(function(response) {
		return response.json();
	})
	.then(function(myJson) {
		return myJson;
	});
}

// Get data of 'aula' (name)
function getDadesAula(aula) {
	return fetch(`${apiUrl}?filter=aula,eq,${aula}&filter=data,bt,${olderDate},${newerDate}`)
	.then(function(response) {
		return response.json();
	})
	.then(function(myJson) {
		return myJson;
	});
}

// Get unique items in an array
function unique(array) {
	return $.grep(array, function(el, index) {
		return index == $.inArray(el, array);
	});
}

// Format date YYYY-MM-DD hh:mm:ss
function formatDate(d) {
	d = new Date(d);
	let DD = d.getDate();
	let MM = d.getMonth()+1;
	let YYYY = d.getFullYear();
	let hh = d.getHours();
	let mm = d.getMinutes();
	let ss = d.getSeconds();

	return `${YYYY}-${MM}-${DD} ${hh}:${mm}:${ss}`
}


